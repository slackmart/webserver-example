Load balanced webserver example
===============================

This is a sample project that shows how to setup a nginx instance as a load balancer for a web application.

Docker images used:
-------------------

- nginx
- python:3.6-alpine

Project tree:
-------------

::

    $ tree
    .
    ├── README.rst
    ├── docker-compose.json
    ├── interviews
    │   ├── Dockerfile
    │   ├── README.rst
    │   ├── docker-compose.json
    │   ├── docker-entrypoint-initdb.d
    │   │   └── initdb.sql
    │   ├── interviews      # This is a submodule, please see the submodule update section
    │   │   ├── VERSION
    │   │   ├── __init__.py
    │   │   ├── __main__.py
    │   │   ├── database.py
    │   │   ├── resources.py
    │   │   └── utils.py
    │   └── requirements.txt
    ├── loadbalancer
    │   ├── Dockerfile
    │   └── nginx.conf
    └── pg_data [error opening dir]

    5 directories, 15 files

Fetch latest code from the interviews submodule
-----------------------------------------------

::

   $ git submodule update --init


Try it yourself:
----------------

::

    $ docker-compose --file docker-compose.json up -d

    $ docker-compose --file docker-compose.json ps
              Name                    Command          State                Ports
    --------------------------------------------------------------------------------
    proj_angrier-api_1          python -m interviews            Up             8000/tcp
    proj_fortune-api_1          python -m interviews            Up             8000/tcp
    proj_nginx-loadbalancer_1   nginx -g daemon off;            Up             0.0.0.0:49010->80/tcp
    proj_postgres_1             docker-entrypoint.sh postgres   Up (healthy)   5432/tcp
    proj_sweetier-api_1         python -m interviews            Up             8000/tcp


See it in action:
-----------------

::

    $ docker-compose --file docker-compose.json logs -f


Open a new terminal and type:
-----------------------------

::

    $ curl --silent http://localhost:49010/v1/interviewer/2/availability | python -m json.tool
    {
        "availability": [],
        "id": 2,
        "name": "Philipp"
    }

    $ cat > availability_payload.json  # Hit enter here and paste the next payload, then press Ctrl+C
    {
        "kind": "weekday", "availability": "14-17", "value": "wednesday,thursday",
        "year": 2018, "month": 11, "week": 2
    }

    $ curl --silent --request PATCH --data @availability_payload.json http://localhost:49010/v1/interviewer/2/availability | python -m json.tool
    {
        "availability": [
            {
                "date": "2018-11-07",
                "hours": "14,15,16"
            },
            {
                "date": "2018-11-08",
                "hours": "14,15,16"
            }
        ],
        "id": 1,
        "name": "Philipp"
    }

    $ for _ in $(seq 1 400); do curl http://localhost:49010/v1/interviewer/2/availability ; done

Look at interviews/README.rst for more examples

What you should see in the docker compose logs:
-----------------------------------------------


::

    sweetier-api_1        | [2018-09-20T03:37:14+00:00] - 172.18.0.2 - "GET /hello-world" - 200 - curl/7.61.0
    sweetier-api_1        | [2018-09-20T03:37:14+00:00] - 172.18.0.2 - "GET /hello-world" - 200 - curl/7.61.0
    nginx-loadbalancer_1  | [20/Sep/2018:03:37:14 +0000]: POST /hello-world HTTP/1.1 from: 172.18.0.1 => to: 172.18.0.3:8000
    nginx-loadbalancer_1  | [20/Sep/2018:03:37:14 +0000]: POST /hello-world HTTP/1.1 from: 172.18.0.1 => to: 172.18.0.5:8000
    nginx-loadbalancer_1  | [20/Sep/2018:03:37:14 +0000]: POST /hello-world HTTP/1.1 from: 172.18.0.1 => to: 172.18.0.4:8000
    nginx-loadbalancer_1  | [20/Sep/2018:03:37:14 +0000]: POST /hello-world HTTP/1.1 from: 172.18.0.1 => to: 172.18.0.5:8000
    fortune-api_1         | [2018-09-20T03:37:14+00:00] - 172.18.0.2 - "GET /hello-world" - 200 - curl/7.61.0
